<!DOCTYPE html>
<html>
    <?php include './php/head.php'; ?>
    <body>
        <?php
        require_once './classes/Story.php';
        $storytime = new Story();
        
        include './php/header.php';
        ?>
        <section class="title">
            <h1>Story Title</h1>
        </section>
        <section class="content">
            <form method="POST" action="./newStory.php?new">
                <input placeholder="What is the story titled?" name="title" class="new" required />
                <input placeholder="first word..." name="word" pattern="[a-zA-Z0-9]{45}" /><input type="submit" value="Submit" class="button" required />
            </form>
        </section>
    </body>
</html>
