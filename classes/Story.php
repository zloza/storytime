<?php
class Story {
    
    private $db_connection = null;
    
    public $errors = array();
    
    public $messages = array();
    
    public function __construct() {
        if (!isset($_SESSION)){
            session_start();
        }
        if (isset($_GET['new'])) {
            if (isset($_SESSION['last_submit']) && (time() - $_SESSION['last_submit'] < 60 * 5)) {
                $this->errors[] = "You must wait 5 minutes between making a submition.";
                $this->errors[] = $_SESSION['last_submit'];
            } else {
                $this->newStory();
            }
        } elseif (isset($_GET['random']) || isset($_GET['id'])) {
            $this->getStory();
//        } elseif (isset($_GET['top']) || isset($_GET['search'])) {
//            $this->listStories();
        } elseif(isset($_GET['contribute'])) {
            if (isset($_SESSION['last_submit']) && (time() - $_SESSION['last_submit'] < 60 * 5)) {
                $this->errors[] = "You must wait 5 minutes between making a submition.";
                $this->errors[] = floor(((60 * 5) - (time() - $_SESSION['last_submit'])) / 60) ." minutes " . (((60 * 5) - (time() - $_SESSION['last_submit'])) % 60) . " seconds";
            } else {
                $this->editStory();
            }
        }
    }
    
    private function dbConnect() {
        $DB_HOST = 'localhost';
        $DB_USER = 'root';
        $DB_PASS = 'root';
        $DB_NAME = 'storytime';
        $this->db_connection = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
        
        if (!$this->db_connection->set_charset("utf8")) {
                $this->errors[] = $this->db_connection->error;
            }
        if (!$this->db_connection->connect_errno) {
            $this->messages[] = "Connected Successfully";
        } else {
            $this->errors[] = "Could Not Connect To Database";
        }
    }
    
    private function newStory() {
        $this->dbConnect();
        $this->checkStory();
        if ($this->checkStory() == true) {
            $title = $_POST['title'];
            $story = '';
            if (!isset($_POST['word'])) {
                $this->errors[] = "First Word Not Given";
            } else {
                $story = $story . $_POST['word'];
            }
            $contributes = 1;
            $active = 0;
            $sql = "INSERT INTO stories (title, story, contributes, active) VALUES ('$title','$story','$contributes','$active')";
            $query_new_story = $this->db_connection->query($sql);
            if ($query_new_story) {
                $this->messages[] = "New Story Created Successfully";
                $_SESSION['last_submit'] = time();
            } else {
                $this->errors[] = "Story Could Not be Created";
            }
        } else {
            $this->errors[] = "Could not check title";
        }
    }
    
    private function checkStory() {
        if (!isset($_POST['title'])) {
            $this->errors[] = "Title is blank";
            return false;
        } else {
            $title = $_POST['title'];
            $sql = "SELECT title FROM stories WHERE title='$title'";
            $query_check_title = $this->db_connection->query($sql);
            if ($query_check_title->num_rows == 1) {
                $this->errors[] = "That title has already been taken. Please choose a new one.";
                return false;
            }
            return true;
        }
    }
    
    private function getStory() {
        $this->dbConnect();
        if (isset($_GET['random'])) {
            $sql = "SELECT id FROM stories ORDER BY id DESC LIMIT 1";
            $query_last_story = $this->db_connection->query($sql);
            $result_last_story = $query_last_story->fetch_object();
            $num_stories = $result_last_story->id;
            $id = rand(1,$num_stories);
        } elseif (isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        $sql = "SELECT id, title, story, contributes FROM stories WHERE id='".$id."'";
        $query_select_story = $this->db_connection->query($sql);
        if ($query_select_story) {
            $result_select_story = $query_select_story->fetch_object();
            
            $_SESSION['id'] = $id;
            $_SESSION['title'] = $result_select_story->title;
            $_SESSION['story'] = $result_select_story->story;
            $_SESSION['contributes'] = $result_select_story->contributes;
        } else {
            $this->errors[] = "Could not find story";
        }
    }
    
    public function listStories() {
        $this->dbConnect();
        if (isset($_GET['top'])) {
            $sql = "SELECT id, title, contributes FROM stories ORDER BY contributes DESC LIMIT 10";
            $query_top_story = $this->db_connection->query($sql);
            $query_list = $query_top_story;
        } elseif (isset($_GET['search'])) {
            $search_query = $_POST['search'];
            $sql = "SELECT id, title, contributes FROM stories WHERE title LIKE '%$search_query%' ORDER BY (CASE "
                    . "WHEN title LIKE '$search_query%' THEN 1 "
                    . "WHEN title LIKE '%$search_query' THEN 3 "
                    . "ELSE 2 "
                    . "END) DESC";
            $query_get_all_stories = $this->db_connection->query($sql);
            $query_list = $query_get_all_stories;
        } elseif (isset($_GET['recent'])) {
            $sql = "SELECT id, title, contributes, timestamp FROM stories ORDER BY timestamp DESC LIMIT 10";
            $query_get_recent_stories = $this->db_connection->query($sql);
            $query_list = $query_get_recent_stories;
        }
        while ($row = $query_list->fetch_object()) {
            echo "<li><a href='./viewStory.php?id=" . $row->id . "'>" . $row->title . "<h5>" . $row->contributes . "</h5></a></li>";
        }
    }
    
    private function editStory() {
        $this->dbConnect();
        
        $title = $_SESSION['title'];
//        $story = $_SESSION['story'];
        $contributes = $_SESSION['contributes'];
        
        $update_story = $_SESSION['story'] . " " . $_POST['word'];
        $contributes++;
        
        $sql = "UPDATE stories SET story='$update_story', contributes='$contributes' WHERE title='$title'";
        $query_update_story = $this->db_connection->query($sql);
        if ($query_update_story) {
            $this->messages[] = "Story Updated Successfully";
            $_SESSION['story'] = $update_story;
            $_SESSION['contributes'] = $contributes;
            $_SESSION['last_submit'] = time();
        } else {
            $this->errors[] = "Story Not Updated";
        }
    }
    
    public function getContributions() {
        $this->dbConnect();
        $sql = "SELECT contributes FROM stories";
        $query_all_stories = $this->db_connection->query($sql);
        $total_contributions = 0;
        while ($row = $query_all_stories->fetch_object()) {
            $total_contributions = $total_contributions + $row->contributes;
        }
        echo $total_contributions;
    }
    
    
}
