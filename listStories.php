<!DOCTYPE html>
<html>
    <?php include './php/head.php'; ?>
    <body>
        <?php
        include './classes/Story.php';
        $storytime = new Story();
        
        include './php/header.php';
        ?>
        <section class="title">
            <h1>Top Stories</h1>
        </section>
        <section class="content">
            <ul class="list more-info">
            <?php $storytime->listStories(); ?>
            </ul>
            <?php
            # Display errors/messages
            if (isset($storytime)) {
                if ($storytime->errors) {
                    foreach ($storytime->errors as $error) {
                        echo "<h6>".$error."</h6>";
                    }
                }
                if ($storytime->messages) {
                    foreach ($storytime->messages as $message) {
                        echo "<h6>".$message."</h6>";
                    }
                }
            }
            ?>
        </section>
    </body>
</html>
