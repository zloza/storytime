<!DOCTYPE html>
<html>
    <?php include './php/head.php'; ?>
    <body>
        <?php
        require_once './classes/Story.php';
        $storytime = new Story();
        
        include './php/header.php';
        ?>
        <section class="title">
            <h1>One-Word-At-a-Time</h1>
            <h2>Crowdsourced stories created one word at a time</h2>
            <h3>Total Contributions: <?php $storytime->getContributions(); ?></h3>
        </section>
        <section class="content">
            <ul class="list">
                <li><form class="search" method="POST" action="./listStories.php?search"><input type="submit" value="Go" class="go" /><input placeholder="Search" name="search" class="list-item" /></form></li>
                <li><a href="./viewStory.php?random" class="go">Go</a><a href="./viewStory.php?random" class="list-item">Random</a></li>
                <li><a href="./listStories.php?top" class="go">Go</a><a href="./listStories.php?top" class="list-item">Top</a></li>
                <li><a href="./listStories.php?recent" class="go">Go</a><a href="./listStories.php?recent" class="list-item">Recent</a></li>
                <li><a href="./newStory.php" class="go">Go</a><a href="./newStory.php" class="list-item">New Story</a></li>
            </ul>
        </section>
    </body>
</html>
