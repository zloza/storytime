<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include './classes/Story.php';
        $storytime = new Story();
        ?>
        <article>
            <?php 
            $storytime->readStory();
            
            ?>
            <form method="POST" action="viewStory.php?contribute">
                <input name="word" placeholder="next word" />
                <input type="submit" value="add word" />
            </form>
            <?php
            # Display errors/messages
            if (isset($storytime)) {
                if ($storytime->errors) {
                    foreach ($storytime->errors as $error) {
                        echo "<h6>".$error."</h6>";
                    }
                }
                if ($storytime->messages) {
                    foreach ($storytime->messages as $message) {
                        echo "<h6>".$message."</h6>";
                    }
                }
            }
            ?>
        </article>
    </body>
</html>
