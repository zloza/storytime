<!DOCTYPE html>
<html>
    <?php include './php/head.php'; ?>
    <body>
        <?php
        require_once './classes/Story.php';
        $storytime = new Story();
        
        include './php/header.php';
        ?>
        <section class="title">
            <h1><?php echo $_SESSION['title']; ?></h1>
            <h4><?php echo $_SESSION['contributes']; ?> contributions</h4>
        </section>
        <section class="content">
            <?php 
            
            ?>
            <div class="p"><?php echo $_SESSION['story']; ?></div>
            <form method="POST" action="./viewStory.php?contribute"><input placeholder="next word..." name="word" /><input type="submit" value="Submit" class="button" /></form>
            <?php
            # Display errors/messages
            if (isset($storytime)) {
                if ($storytime->errors) {
                    foreach ($storytime->errors as $error) {
                        echo "<h6>".$error."</h6>";
                    }
                }
                if ($storytime->messages) {
                    foreach ($storytime->messages as $message) {
                        echo "<h6>".$message."</h6>";
                    }
                }
            }
            ?>
        </section>
    </body>
</html>
